<script src="assets/js/modernizr.js"></script>

<div id="messenger">
  <div id="m_titlePanel">
    <h3 id="m_title">Leave A Comment</h3>
  </div>
  <div id="m_messagePanel">
    <?php
      require('loadComments.php');
    ?>
  </div>
  <div id="m_commentPanel">
  	<div id="m_commentBox">
      <button id="m_refresh">
        Refresh
      </button>
  		<textarea id="m_comment" type="text" name="comment" placeholder="Comment"></textarea>
  		<button id="m_commentSubmit">
  			Send
  		</button>
  		<button id="m_info">
  			Info
  		</button>
  	</div>
  </div>
</div>

<div id="m_infoPanel">
	<div id="m_infoBox">
		<h5 id="m_infoNameLabel">Enter Name:</h5>
		<input id="m_infoName" type="text" name="na me" placeholder="Type Name" />
		<button id="m_infoConfirm">
			Confirm
		</button>
		<button id="m_infoCancel">
			Cancel
		</button>
	</div>
</div>

<script src="assets/js/jquery-1.10.1.min.js"></script>
<script src="assets/js/foundation.min.js"></script>

<script>
  $(document).foundation();
</script>