$(document).ready(function() {

	var name = "";

	// Send Comment Button
	$('#m_commentSubmit').click(function() {

    if($('#m_comment').val() == "") {
      return;
    }

		// Make sure the sender has entered a name
		if(name == "") {

			$('#m_infoPanel').fadeIn();
			return;
		}

    // get the message from the textarea
    var message = $('#m_comment').val();

		// Submit comment to server
		$.ajax({
  			type: 'POST',
  			url: "saveComment.php",
  			data: {comment: {name: name, message: message}},
  			success: function(data) {
          $('#m_comment').val("");
  			}
  		});
	});

  // Refresh Button
  $('#m_refresh').click(function() {

    // Get comments from server
    $.ajax({
        type: 'POST',
        url: "loadComments.php",
        complete: function(data) {
          console.log(data);
          $('#m_messagePanel').empty();
          $('#m_messagePanel').append(data['responseText']);
        }
      });
  });

	// Info button
	$('#m_info').click(function() {

		$('#m_infoPanel').fadeIn();
	});

	// Info Confirm
	$('#m_infoConfirm').click(function() {

		// Change the name to the confirmed value
		name = $("#m_infoName").val();
		$('#m_infoPanel').fadeOut();
	});


	// Info Cancel
	$('#m_infoCancel').click(function() {

		// Cancel Changes
		$('#m_infoPanel').fadeOut();
		$("#m_infoName").val(name);
	});
});