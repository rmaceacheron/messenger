<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Messenger App</title>
    <link rel="stylesheet" href="assets/css/foundation.css" />
    <link rel="stylesheet" href="assets/css/messenger.css" />
    
  </head>
  <body>
    
    <?php
      require_once('messenger.php');
    ?>    
    

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/messenger.js"></script>
  </body>
</html>
